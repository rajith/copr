from paper3.data_wrangling import get_filtered_dataframes
from paper3.data_wrangling import get_OTU_wise_dataframes
from paper3.data_wrangling import calculate_dwts
from paper3.data_wrangling import interpolate_dwts
from paper3.data_wrangling import calc_distances

from paper3.visualisations import visualise_dwt
from paper3.visualisations import visualise_interpolated_dwt
from paper3.visualisations import visualise_distances

from paper3.tools import dists_to_dataframe

from sklearn.metrics import mean_squared_error

paths_faecal_and_oral = ['../Data/processed/M3_feces_L5.csv', '../Data/processed/F4_feces_L5.csv', '../Data/processed/M3_tongue_L5.csv', '../Data/processed/F4_tongue_L5.csv']
paths_hands = ['../Data/processed/M3_R_palm_L5.csv', '../Data/processed/M3_L_palm_L5.csv', '../Data/processed/F4_R_palm_L5.csv', '../Data/processed/F4_L_palm_L5.csv']

paths_neonatal = []
for i in range(0, 20):
    paths_neonatal.append('../Data/raw_la_rosa2014/'+str(i)+'.csv')

# Get Filtered Dataframes, and Intersection OTU List
dfs, U = get_filtered_dataframes(paths_neonatal, 10, 0)

# Convert Dataframes to OTU-wise format
dfs = get_OTU_wise_dataframes(dfs, U)
#print(OTU_dfs[4].index)

# Calculate Discrete Wavelet Transform
dfs = calculate_dwts(dfs)
print(len(dfs))

# Visualise DWTs
visualise_dwt(dfs, U)

# Interpolate DWTs
dfs = interpolate_dwts(dfs)

# Visualise Interpolated DWTs
visualise_interpolated_dwt(dfs, U)

# Calculate Distances
dists = calc_distances(dfs)

# Visualise Distances
visualise_distances(dists_to_dataframe(dfs, dists), U)

# Alignment


# plot aligned B-Splines for each bacteria from all the dfs

# Filter off non-aligned B-Splines

# Repeat previous plot

# Plot all the bacteria for all the dfs




