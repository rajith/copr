from paper2.dataset import Dataset
from paper2.ga import run_ga
from paper2.ga import test_ga
from paper2.MIN import MIN
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate as interpolate
import warnings
import pydot as pyd
import seaborn as sns
from tslearn.clustering import TimeSeriesKMeans
from tslearn.datasets import CachedDatasets
from tslearn.preprocessing import TimeSeriesScalerMeanVariance, TimeSeriesResampler
from tslearn.metrics import soft_dtw
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_samples, silhouette_score
from sklearn.mixture import GaussianMixture as GMM
from sklearn import metrics
from collections import Counter

from bokeh.palettes import Category20, Category10

from sklearn.metrics import mean_squared_error
import pywt
import umap
from scipy import integrate

from paper3.tools import import_datasets
from paper3.tools import intersection
from paper3.tools import filter_by_row_index
from paper3.tools import distance
from paper3.tools import SelBest
from paper3.tools import OverlapCoefficient
from paper3.tools import JaccardIndex

from pyloess.Loess import Loess

import statistics as stat


# take input from csv files (with limits to number of OTUs and time points)
# get the intersection of the OTU lists from each sample
# Filter the OTUs from the intersection (U), and covert data to floats
def get_filtered_dataframes(paths, count=10, delta_t=0):
    dfs = import_datasets(paths, count, delta_t)
    u = intersection(dfs)
    u = list(u)

    new_dfs = filter_by_row_index(dfs, u)
    for i in range(len(new_dfs)):
        new_dfs[i].columns = new_dfs[i].columns.astype(float)

    return new_dfs, u


# Convert the Sample-wise Dataframes into OTU-wise dataframes
def get_OTU_wise_dataframes(new_dfs, u):
    OTU_dfs = []
    for OTU in u:
        # print(OTU)
        df_local = pd.DataFrame(columns=['subject', 'x', 'y'])
        for i, df in enumerate(new_dfs):
            if len(df.columns) > 3:
                # plt.plot(df.loc[OTU], marker='x', color=colors[i%len(colors)], label=paths[i])
                df_local = df_local.append(
                    {'subject': i, 'x': list(new_dfs[i].loc[OTU].index), 'y': list(new_dfs[i].loc[OTU].values)},
                    ignore_index=True)
        OTU_dfs.append(df_local)

    return OTU_dfs


# Cubic interpolation of raw data. Stores a interpolate object in the OTU-wise dataframe
def interpolate_raw(OTU_dfs):
    for i, OTU in enumerate(OTU_dfs):
        inters = []
        for j in OTU_dfs[i].index:
            y = OTU_dfs[i].loc[j].y
            x = OTU_dfs[i].loc[j].x
            inter = interpolate.interp1d(x, y, kind='cubic')
            inters.append(inter)
        OTU_dfs[i]['inter_raw'] = inters

    return OTU_dfs


# calculate the discreet wavelet transforms
# Drop rows where the DWT resulted in less than four time points
def calculate_dwts(OTU_dfs):
    for i, OTU in enumerate(OTU_dfs):
        cAs = []
        cDs = []
        for j in OTU_dfs[i].index:
            (cA, cD) = pywt.dwt(OTU_dfs[i].loc[j].y, 'sym2')
            cAs.append(cA)
            cDs.append(cD)
        OTU_dfs[i]['cA'] = cAs
        OTU_dfs[i]['cD'] = cDs

    for i, OTU in enumerate(OTU_dfs):
        for j in OTU.index:
            if len(OTU_dfs[i].loc[j].cA) < 4:
                OTU_dfs[i] = OTU_dfs[i].drop(index=[j])
        OTU_dfs[i] = OTU_dfs[i].reset_index(drop=True)

    return OTU_dfs


# Cubic interpolation of dwts. Stores a interpolate object in the OTU-wise dataframe
def interpolate_dwts(OTU_dfs):
    for i, OTU in enumerate(OTU_dfs):
        inters = []
        for j in OTU_dfs[i].index:
            cA = OTU_dfs[i].loc[j].cA
            x = np.arange(0, len(cA), 1)
            inter = interpolate.interp1d(x, cA, kind='cubic')
            inters.append(inter)
        OTU_dfs[i]['inter'] = inters

    return OTU_dfs


def resample_and_scaler_mean_variance(OTU_dfs, sz=40):
    for i, OTU in enumerate(OTU_dfs):
        Ys = []
        for j in OTU_dfs[i].index:
            cA = OTU_dfs[i].loc[j].cA
            inter = OTU_dfs[i].loc[j].inter
            xnew = np.arange(0, len(cA) - 1 + 0.1, 0.1)
            ynew = inter(xnew)
            ynew = TimeSeriesResampler(sz=100).fit_transform(ynew)
            Ys.append(ynew)
        Ys = np.vstack(Ys)

        Ys = TimeSeriesScalerMeanVariance().fit_transform(Ys)
        # Make time series shorter
        Ys = TimeSeriesResampler(sz=sz).fit_transform(Ys)
        OTU_dfs[i]['resampled'] = Ys.tolist()

    return OTU_dfs


# B-Spline interpolation of dwts. Stores a BSpline object in the OTU-wise dataframe
# def BSpline_dwts(OTU_dfs):
#     for i, OTU in enumerate(OTU_dfs):
#         inters = []
#         for j in OTU_dfs[i].index:
#             cA = OTU_dfs[i].loc[j].cA
#             x = np.arange(0, len(cA), 1)
#             inter = #TODO
#             inters.append(inter)
#         OTU_dfs[i]['bspline'] = inters
#
#     return OTU_dfs


# Calculate the distance between all the curves
def calc_distances(OTU_dfs):
    N = len(OTU_dfs[0].index)
    OTU_dists = []
    for i, OTU in enumerate(OTU_dfs):
        dists = np.zeros((N, N))
        for j in OTU_dfs[i].index:
            for k in OTU_dfs[i].index:
                if (j >= k):
                    continue
                else:
                    dists[k][j] = dists[j][k] = distance(OTU_dfs[i].loc[j], OTU_dfs[i].loc[k])

        OTU_dists.append(dists)

    return OTU_dists

def normalised_soft_dtw(x, y, gamma):
    return soft_dtw(x, y, gamma=gamma) - (soft_dtw(x, x, gamma=gamma) + soft_dtw(y, y, gamma=gamma))/2

def calc_softdtw_distances(OTU_dfs, gamma=0.01, normalised=True):
    N = len(OTU_dfs[0].index)
    OTU_dists = []
    for i, OTU in enumerate(OTU_dfs):
        dists = np.zeros((N, N))
        for j in OTU_dfs[i].index:
            for k in OTU_dfs[i].index:
                if (j >= k):
                    continue
                else:
                    if normalised:
                        dists[k][j] = dists[j][k] = normalised_soft_dtw(OTU_dfs[i].loc[j].resampled, OTU_dfs[i].loc[k].resampled, gamma=gamma)
                    else:
                        dists[k][j] = dists[j][k] = soft_dtw(OTU_dfs[i].loc[j].resampled, OTU_dfs[i].loc[k].resampled, gamma=gamma)
        dists = dists.clip(0)
        OTU_dists.append(dists)

    return OTU_dists

def get_umap_embeddings(distance_matrices):
    umap_embeddings = []
    for distance_matrix in distance_matrices:
        reducer_1 = umap.UMAP(metric="precomputed")
        embedding_1 = reducer_1.fit_transform(distance_matrix)
        umap_embeddings.append(embedding_1)

    return umap_embeddings


# umap_embeddings: x,y coordinates for the umap scatter plots
# dfs: OTU-wise dataframe with various columns. Examine columns through dfs[i].head()
def df_for_columndatasource(umap_embeddings, dfs, **kwargs):
    df_cds = pd.DataFrame()
    for i, embedding in enumerate(umap_embeddings):
        dimred_Df = pd.DataFrame(data=embedding, columns=["x{0}".format(i), "y{0}".format(i)])
        if (i == 0):
            df_cds = dimred_Df
        else:
            df_cds = df_cds.join(dimred_Df)

    for i, df in enumerate(dfs):
        df_cds = df_cds.join(df.resampled).rename(columns={"resampled": "resampled{0}".format(i)})

    #TODO: change 40
    df_cds['xs'] = [np.arange(40)] * len(df_cds['x0'])

    df_xs_and_ys = pd.DataFrame()

    for i, df in enumerate(dfs):
        xs = []
        ys = []
        for j in df.index:
            cA = df.loc[j].cA
            x = np.arange(0, len(cA), 1)
            inter = df.loc[j].inter
            xnew = np.arange(0, len(cA) - 1 + 0.1, 0.1)
            ynew = inter(xnew)
            xs.append(xnew)
            ys.append(ynew)
        xs = pd.Series(xs)
        ys = pd.Series(ys)

        df_xs_and_ys.insert(loc=2 * i, column="xs_dtw{0}".format(i), value=xs)
        df_xs_and_ys.insert(loc=2 * i + 1, column="ys_dtw{0}".format(i), value=ys)

    df_cds = df_cds.join(df_xs_and_ys)

    df_cds.insert(loc=0, column="subject", value=dfs[0].subject)

    if "kmeans" in kwargs:
        colors_bokeh = Category20[20]
        clusters = kwargs["kmeans"]
        inc = 0
        for i, cluster in enumerate(clusters):
            colors_list = []
            for label in cluster.labels_:
                colors_list.append(colors_bokeh[(inc+label) % 20])
            inc += len(set(cluster.labels_))
            colors = pd.Series(colors_list)
            df_cds.insert(loc=0, column="kmeans{0}".format(i), value=colors)

    if "gmm" in kwargs:
        colors_bokeh = Category20[20]
        clusters = kwargs["gmm"]
        inc = 0
        for i, cluster in enumerate(clusters):
            colors_list = []
            for label in cluster:
                colors_list.append(colors_bokeh[(inc+label) % 20])
            inc += len(set(cluster))

            colors = pd.Series(colors_list)
            df_cds.insert(loc=0, column="gmm{0}".format(i), value=colors)


    return df_cds


# n_clusters > 0 means force the number of clusters
# Otherwise use the silhouette score to get the optimal number of clusters
# TODO: remove U
def get_kmeans_clustering(U, umap_embeddings, n_clusters=0, n_clusters_min=2, n_clusters_max=10):
    kmeans_ = []

    if n_clusters > 0:
        n_clusters_min = n_clusters
        n_clusters_max = n_clusters + 1

    for i, embedding in enumerate(umap_embeddings):
        best_kmeans = None
        best_sil_score = 0
        best_n_cluster = 0
        for j in range(n_clusters_min, n_clusters_max):
            kmeans = KMeans(n_clusters=j, random_state=0).fit(embedding)
            silhouette_avg = silhouette_score(embedding, kmeans.labels_)
            #print(U[i], j, silhouette_avg)
            if(silhouette_avg > best_sil_score):
                best_kmeans = kmeans
                best_sil_score = silhouette_avg
                best_n_cluster = j
        kmeans_.append(best_kmeans)
        #print("*********", U[i], best_n_cluster, best_sil_score)

    return kmeans_

# n_clusters > 0 means force the number of clusters
# Otherwise use the silhouette score to get the optimal number of clusters
# TODO: remove U
def get_gmm_clustering(U, umap_embeddings, n_clusters=0, n_clusters_min=2, n_clusters_max=10):
    gmm_ = []
    gmm_labels_ = []
    sils_ = []
    sils_err_ = []

    if n_clusters > 0:
        n_clusters_min = n_clusters
        n_clusters_max = n_clusters + 1

    for i, embeddings in enumerate(umap_embeddings):
        n_clusters = np.arange(n_clusters_min, n_clusters_max)
        sils = []
        sils_err = []
        iterations = 20
        best_sil_score = 0
        best_gmm = None
        best_label = None
        for n in n_clusters:
            tmp_sil = []
            for _ in range(iterations):
                gmm = GMM(n, n_init=2).fit(embeddings)
                labels = gmm.predict(embeddings)
                sil = metrics.silhouette_score(embeddings, labels, metric='euclidean')
                tmp_sil.append(sil)
            val = np.mean(SelBest(np.array(tmp_sil), int(iterations / 5)))
            if(val > best_sil_score):
                best_sil_score = val
                best_gmm = GMM(n, n_init=2).fit(embeddings)
                best_label = best_gmm.predict(embeddings)
            err = np.std(tmp_sil)
            sils.append(val)
            sils_err.append(err)
        sils_.append(sils)
        sils_err_.append(sils_err)
        gmm_.append(best_gmm)
        gmm_labels_.append(best_label)

    return gmm_, gmm_labels_, sils_, sils_err_

# Create and visualise clusters according to the interpolated curves.
# Returns silhouette score and ypred (predicted cluster numbers as an array)
# U - list of bacteria names
# i - which bacteria number to look at
# n_clusters - default:3 number of clusters to be generated
# seed - default:0 for the Kmeans random seed
# verbose - default:False, for the k-means clustering verbose setting
# visualise - default:True, whether or not to plot the clusters
#TODO : Remove Visualisation parts and take that to visualisations.py

# Bokeh Libraries
from bokeh.io import output_notebook, output_file, reset_output
from bokeh.plotting import figure, show
from bokeh.palettes import Category20, Category10
from bokeh.models import HoverTool
from bokeh.models import Panel, Tabs
from bokeh.layouts import row
import numpy as np

import holoviews as hv
from holoviews import opts

hv.extension('bokeh')

colors_bokeh = Category10[10]


def interpolated_clusters(OTU_dfs, U, i, n_clusters=3, seed=0, verbose=False, visualise=True, output="../Data/clusters.html"):

    if output is None:
        reset_output()
        output_notebook()
    else:
        output_file(output, title="Time-Series Clustering")

    np.random.seed(seed)



    Ys = []
    for j in OTU_dfs[i].index:
        cA = OTU_dfs[i].loc[j].cA
        x = np.arange(0, len(cA), 1)
        inter = OTU_dfs[i].loc[j].inter
        xnew = np.arange(0, len(cA) - 1 + 0.1, 0.1)
        ynew = inter(xnew)
        ynew = TimeSeriesResampler(sz=100).fit_transform(ynew)
        Ys.append(ynew)
    Ys = np.vstack(Ys)

    Ys = TimeSeriesScalerMeanVariance().fit_transform(Ys)
    print(Ys.shape)
    # Make time series shorter
    Ys = TimeSeriesResampler(sz=40).fit_transform(Ys)
    sz = Ys.shape[1]

    # Soft-DTW-k-means

    #print("Soft-DTW k-means")
    sdtw_km = TimeSeriesKMeans(n_clusters=n_clusters,
                               metric="softdtw",
                               metric_params={"gamma": .01},
                               verbose=verbose,
                               random_state=seed)

    X_train = Ys
    y_pred = sdtw_km.fit_predict(X_train)

    sil_score = silhouette_score(X_train, y_pred, metric="softdtw")

    #UMAP Dimred

    distance_matrix = np.zeros(shape=(X_train.shape[0], X_train.shape[0]))
    for i1, x1 in enumerate(X_train):
        for i2, x2 in enumerate(X_train):
            distance_matrix[i1, i2] = soft_dtw(x1, x2, gamma=0.01)

    distance_matrix = distance_matrix.clip(0)

    reducer_1 = umap.UMAP(metric="precomputed")
    embedding_1 = reducer_1.fit_transform(distance_matrix)
    dimred_Df_1 = pd.DataFrame(data=embedding_1, columns=['x', 'y'])
    dimred_Df_1['color'] = [colors_bokeh[x] for x in y_pred]




    tabs = []

    for yi in range(n_clusters):
        # Set up a generic figure() object
        fig = figure(title='Cluster {0} : {1} Samples'.format((yi + 1), len(X_train[y_pred == yi])), plot_width=600, plot_height=600, background_fill_color="#fafafa")
        fig.xaxis.axis_label = "Temporal Axis"
        fig.yaxis.axis_label = "Amplitude"

        for xx in X_train[y_pred == yi]:
            _y = xx.ravel()
            _x = np.arange(_y.size)
            line = fig.line(_x, _y, color='gainsboro')


            # line = fig.line(range(0, len(cA)), cA, color=colors_bokeh[j % len(colors_bokeh)], name=str(j),
            #                legend_label="Approximation: Sample" + str(j))
            #fig.add_tools(HoverTool(renderers=[line], tooltips=[('Sample', str(j))]))

        #fig.legend.click_policy = "hide"
        #fig.add_layout(fig.legend[0], 'right')

        _y = sdtw_km.cluster_centers_[yi].ravel()
        _x = np.arange(_y.size)
        fig.line(_x, _y, color=colors_bokeh[yi])

        tab = Panel(child=fig, title="Cluster {0}".format(yi + 1))
        tabs.append(tab)

    tabs_ = Tabs(tabs=tabs)

    scatter_figure = figure(title="UMAP Dimention Reduction - {0}".format(U[i]), plot_width=600, plot_height=600, background_fill_color="#fafafa")
    scatter_figure.xaxis.axis_label = "Dimension 1"
    scatter_figure.yaxis.axis_label = "Dimension 2"

    scatter_figure.scatter(dimred_Df_1.x, dimred_Df_1.y, fill_color=dimred_Df_1.color, line_color=None, radius=0.1)

    show(row(scatter_figure, tabs_))

    # for yi in range(n_clusters):
    #     # plt.subplot(3, 3, 7 + yi)
    #     for xx in X_train[y_pred == yi]:
    #         plt.plot(xx.ravel(), "k-", alpha=.2)
    #     plt.plot(sdtw_km.cluster_centers_[yi].ravel(), "r-")
    #     # plt.xlim(0, sz)
    #     # plt.ylim(-4, 4)
    #     plt.text(0.55, 0.85, 'Cluster {0} : {1} Samples'.format((yi + 1), len(X_train[y_pred == yi])),
    #              transform=plt.gca().transAxes)
    #     if yi == 0:
    #         plt.title("Soft-DTW $k$-means")
    #     plt.show()

    return sil_score, y_pred
import holoviews as hv
def calculate_cluster_membership(U, gmm_labels):

    names = []
    for u in U:
        name = None
        _pos = u[::-1].find('_')
        if _pos == -1:
            name = u
        elif _pos == 0:
            name = "Undefined"
        else:
            name = (u[-_pos:]).replace('|', '')
            if len(name) < 0:
                name = "Undefined"
        names.append(name)

    cluster_members = []
    cluster_names = []
    for k, OTU in enumerate(gmm_labels):
        num = len(set(OTU))
        for i in np.arange(num):
            indexes = [j for j, x in enumerate(OTU) if x == i]
            cluster_members.append(set(indexes))
            cluster_names.append(names[k] + " " + str(i))
            #cluster_names.append(str(k)+" "+str(i))
            #print(U[k] + " " + str(i))
            #print(set(indexes))

    num = len(cluster_names)

    overlap = []
    jaccard = []

    for i in range(num):
        for j in range(num):
            if j>=i:
                break
            temp = (cluster_names[i], cluster_names[j], OverlapCoefficient(cluster_members[i], cluster_members[j]))
            temp2 = (cluster_names[i], cluster_names[j], JaccardIndex(cluster_members[i], cluster_members[j]))
            overlap.append(temp)
            jaccard.append(temp2)

    return overlap, jaccard


def df_for_medians_columndatasource(U, df_cds):
    timepoints = len(df_cds['xs'][0])
    medians_resampled = []
    for k, OTU in enumerate(U):
        median_resampled = []
        for i in range(timepoints):
            # Get the median of the i-th timepoint across all the subjects for the k-th OTU. sublist code is for flattening the array
            median_resampled.append(stat.median([item for sublist in [a[i] for a in df_cds['resampled{0}'.format(k)]] for item in sublist]))
        medians_resampled.append(median_resampled)

    df_medians = pd.DataFrame(data={'xs': df_cds['xs'][0]})
    for k, OTU in enumerate(U):
        df_medians['resampled{0}'.format(k)] = medians_resampled[k]

    return df_medians
