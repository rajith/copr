from paper2.dataset import Dataset
import numpy as np
import pandas as pd
from scipy import integrate


# intersection of the index values of the dfs
def intersection(dfs):
    s_meta = []
    for df in dfs:
        s_meta.append(set(sorted((df.df.index.values.tolist()))))
    return set.intersection(*s_meta)


# import dataset and select time interval and ranks
def import_dataset(path, count=0, start=0, delta_t=0):
    df = Dataset.from_csv(path)
    if count:
        df = df.select_by_rank(count=count)
    if delta_t:
        df = df.time_interval(start=start, delta_t=delta_t)
    return df


# import multiple datasets and return a list of Datasets
def import_datasets(paths, count=0, start=0, delta_t=0):
    dfs = []
    for path in paths:
        df = import_dataset(path, count=count, start=start, delta_t=delta_t)
        dfs.append(df)
    return dfs


# filter by row index, using an index set
def filter_by_row_index(dfs, U):
    new_dfs = []
    for df in dfs:
        df = df.df
        new_dfs.append(df[df.index.isin(U)])

    return new_dfs


# input two curves, output distance between the two (continuous)
# input: two rows from an OTU-specific abundance profile. Each row is representative of a different subject/sample
def distance(curve1, curve2):
    # calculation of x_min and x_max needs to be change in the future to accommodate linear transformations.
    x_min = 0
    x_max = min(len(curve1.cA), len(curve2.cA)) - 1
    x_range = np.arange(x_min, x_max + 0.1, 0.1)

    curve1_pts = curve1.inter(x_range)
    curve2_pts = curve2.inter(x_range)

    idx = np.argwhere(np.diff(np.sign(curve1_pts - curve2_pts))).flatten()
    arr = x_range[idx]
    arr = np.insert(arr, 0, x_min)
    arr = np.append(arr, x_max)

    int_sum = 0
    for i, num in enumerate(arr[:-1]):
        low = arr[i]
        high = arr[i + 1]
        y, err = integrate.quad(lambda x: ((curve1.inter(x) - curve2.inter(x))), low, high)
        # print(i, y, err)
        int_sum += abs(y)

    return int_sum / (x_max - x_min)


# input two curves, output distance between the two (discrete)
# TODO

# Dataframe in rows format - for visualisation with HoloViz
def dists_to_dataframe(OTU_dfs, OTU_dists):
    OTU_dists_dfs_row_form = []
    for i, OTU in enumerate(OTU_dfs):
        OTU_dists_df = pd.DataFrame(columns=['Sample1', 'Sample2', 'Distance'])
        for j in OTU_dfs[i].index:
            for k in OTU_dfs[i].index:
                OTU_dists_df = OTU_dists_df.append({'Sample1': j, 'Sample2': k, 'Distance': OTU_dists[i][j][k]},
                                                   ignore_index=True)
        OTU_dists_dfs_row_form.append(OTU_dists_df)

    return OTU_dists_dfs_row_form


# from https://github.com/vlavorini/ClusterCardinality/
def SelBest(arr: list, X: int) -> list:
    '''
    returns the set of X configurations with shorter distance
    '''
    dx = np.argsort(arr)[:X]
    return arr[dx]

# returns the overlap co-efficient
# A.K.A. Szymkiewicz–Simpson coefficient
def OverlapCoefficient(set1, set2):
    return float(len(set1.intersection(set2))) / float(min(len(set1), len(set2)))
# returns the jacard index
def JaccardIndex(set1, set2):
    return float(len(set1.intersection(set2))) / float(len(set1.union(set2)))