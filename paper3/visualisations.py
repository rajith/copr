# Bokeh Libraries
from bokeh.io import output_notebook, output_file, reset_output
from bokeh.plotting import figure, show
from bokeh.palettes import Category20, Category10
from bokeh.models import HoverTool
from bokeh.models import ColumnDataSource, Panel, Tabs, Ellipse, CustomJS
from bokeh.layouts import gridplot, row
import holoviews as hv

import numpy as np
import pandas as pd
import math

import holoviews as hv
from holoviews import opts

hv.extension('bokeh')

colors_bokeh = Category20[20]


# Visualisation of Raw Data
def visualise_raw(OTU_dfs, U, legend="below", output="../Data/raw.html"):
    if output is None:
        reset_output()
        output_notebook()
    else:
        output_file(output, title="Raw Data")
    tabs = []

    for i, OTU in enumerate(OTU_dfs):
        # Set up a generic figure() object
        fig = figure(title=U[i], plot_width=1200, plot_height=600, background_fill_color="#fafafa")
        fig.xaxis.axis_label = "Temporal Axis"
        fig.yaxis.axis_label = "Amplitude"

        for j in OTU_dfs[i].index:
            x = OTU_dfs[i].loc[j].x
            y = OTU_dfs[i].loc[j].y
            subject = OTU_dfs[i].loc[j].subject
            line = fig.line(x, y, color=colors_bokeh[j % len(colors_bokeh)], name=str(j),
                            legend_label="Approximation: Sample" + str(j))
            fig.add_tools(HoverTool(renderers=[line], tooltips=[('Sample', str(subject)), ('Index', str(j))]))

        fig.legend.click_policy = "hide"
        fig.add_layout(fig.legend[0], legend)
        tab = Panel(child=fig, title=U[i])
        tabs.append(tab)

    tabs_ = Tabs(tabs=tabs)
    show(tabs_)

    return


# Visualisation of the interpolated curves
def visualise_interpolated_raw(OTU_dfs, U, output="../Data/interpolated_raw.html"):
    if output is None:
        reset_output()
        output_notebook()
    else:
        output_file(output, title="Interpolated Raw Curves")
    tabs = []

    for i, OTU in enumerate(OTU_dfs):
        # Set up a generic figure() object
        fig = figure(title=U[i], plot_width=1200, plot_height=600, background_fill_color="#fafafa")
        fig.xaxis.axis_label = "Temporal Axis"
        fig.yaxis.axis_label = "Amplitude"

        for j in OTU_dfs[i].index:
            y = OTU_dfs[i].loc[j].y
            x = OTU_dfs[i].loc[j].x
            inter = OTU_dfs[i].loc[j].inter_raw
            xnew = np.arange(x[0], x[-1] - 0.1, 0.1)
            ynew = inter(xnew)
            subject = OTU_dfs[i].loc[j].subject
            circs = fig.circle(x, y, fill_color=colors_bokeh[j % len(colors_bokeh)])
            line = fig.line(xnew, ynew, color=colors_bokeh[j % len(colors_bokeh)], name=str(j),
                            legend_label="Approximation: Subject" + str(j))
            fig.add_tools(HoverTool(renderers=[line], tooltips=[('Sample', str(subject)), ('Index', str(j))]))

        fig.legend.click_policy = "hide"
        fig.add_layout(fig.legend[0], 'right')
        tab = Panel(child=fig, title=U[i])
        tabs.append(tab)

    tabs_ = Tabs(tabs=tabs)
    show(tabs_)

    return



# Visualisation of Discrete Wavelet Transforms
def visualise_dwt(OTU_dfs, U, legend="below", output="../Data/dwt.html"):
    if output is None:
        reset_output()
        output_notebook()
    else:
        output_file(output, title="Discrete Wavelet Transform")
    tabs = []

    for i, OTU in enumerate(OTU_dfs):
        # Set up a generic figure() object
        fig = figure(title=U[i], plot_width=1200, plot_height=600, background_fill_color="#fafafa")
        fig.xaxis.axis_label = "Temporal Axis"
        fig.yaxis.axis_label = "Amplitude"

        for j in OTU_dfs[i].index:
            cA = OTU_dfs[i].loc[j].cA
            subject = OTU_dfs[i].loc[j].subject
            line = fig.line(range(0, len(cA)), cA, color=colors_bokeh[j % len(colors_bokeh)], name=str(j),
                            legend_label="Approximation: Sample" + str(j))
            fig.add_tools(HoverTool(renderers=[line], tooltips=[('Sample', str(subject)), ('Index', str(j))]))

        fig.legend.click_policy = "hide"
        fig.add_layout(fig.legend[0], legend)
        tab = Panel(child=fig, title=U[i])
        tabs.append(tab)

    tabs_ = Tabs(tabs=tabs)
    show(tabs_)

    return


def visualise_interpolated_dwt(OTU_dfs, U, output="../Data/interpolated_dfs.html"):
    if output is None:
        reset_output()
        output_notebook()
    else:
        output_file(output, title="Interpolated DWT Curves")
    tabs = []

    for i, OTU in enumerate(OTU_dfs):
        # Set up a generic figure() object
        fig = figure(title=U[i], plot_width=1200, plot_height=600, background_fill_color="#fafafa")
        fig.xaxis.axis_label = "Temporal Axis"
        fig.yaxis.axis_label = "Amplitude"

        for j in OTU_dfs[i].index:
            cA = OTU_dfs[i].loc[j].cA
            x = np.arange(0, len(cA), 1)
            inter = OTU_dfs[i].loc[j].inter
            xnew = np.arange(0, len(cA) - 1 + 0.1, 0.1)
            ynew = inter(xnew)
            subject = OTU_dfs[i].loc[j].subject
            circs = fig.circle(range(0, len(cA)), cA, fill_color=colors_bokeh[j % len(colors_bokeh)])
            line = fig.line(xnew, ynew, color=colors_bokeh[j % len(colors_bokeh)], name=str(j),
                            legend_label="Approximation: Subject" + str(j))
            fig.add_tools(HoverTool(renderers=[line], tooltips=[('Sample', str(subject)), ('Index', str(j))]))

        fig.legend.click_policy = "hide"
        fig.add_layout(fig.legend[0], 'right')
        tab = Panel(child=fig, title=U[i])
        tabs.append(tab)

    tabs_ = Tabs(tabs=tabs)
    show(tabs_)

    return


# Holoviz Visualisation
def visualise_distances(OTU_dists_dfs_row_form, U, output="../Data/distances.html"):
    if output is None:
        reset_output()
        output_notebook()
    else:
        output_file(output, title="Distances")
    hm = None
    for i, df_row in enumerate(OTU_dists_dfs_row_form):
        if i == 0:
            hm = hv.HeatMap(df_row, label=U[i])
        else:
            hm = hm + hv.HeatMap(df_row, label=U[i])
    plot = hm.opts(opts.HeatMap(tools=['hover'], cmap='BuPu', colorbar=True, ))
    show(hv.render(plot))
    return

# Visualise UMAP embedding
def visualise_umap(umap_embeddings, U, output="../Data/umap.html", radius=0.05):
    if output is None:
        reset_output()
        output_notebook()
    else:
        output_file(output, title="UMAP")

    tabs = []
    for i, embedding in enumerate(umap_embeddings):
        scatter_figure = figure(title="UMAP Dimention Reduction", plot_width=1200, plot_height=600,
                                background_fill_color="#fafafa")
        scatter_figure.xaxis.axis_label = "Dimension 1"
        scatter_figure.yaxis.axis_label = "Dimension 2"

        dimred_Df_1 = pd.DataFrame(data=embedding, columns=['x', 'y'])

        scatter_figure.scatter(dimred_Df_1.x, dimred_Df_1.y, fill_color="Black", line_color=None, radius=radius)

        tabs.append(Panel(child=scatter_figure, title=U[i]))

    show(Tabs(tabs=tabs))


# Visualise UMAP embeddings, linked across the OTUs by shared Subject/Sample ID
def visualise_umap_linked(umap_embeddings, U, output="../Data/umap_linked.html", ncols=4, plot_width=250, plot_height=250):
    if output is None:
        reset_output()
        output_notebook()
    else:
        output_file(output, title="Linked UMAP")

    df_cds = pd.DataFrame()
    for i, embedding in enumerate(umap_embeddings):
        dimred_Df = pd.DataFrame(data=embedding, columns=["x{0}".format(i), "y{0}".format(i)])
        if (i == 0):
            df_cds = dimred_Df
        else:
            df_cds = df_cds.join(dimred_Df)

    source = ColumnDataSource(df_cds)
    TOOLS = "box_select,lasso_select,hover,help"

    tabs = []
    for i, embedding in enumerate(umap_embeddings):
        fig = figure(tools=TOOLS, title=U[i], background_fill_color="#fafafa")
        fig.circle("x{0}".format(i), "y{0}".format(i), size=4, hover_color="firebrick", source=source)
        tabs.append(fig)

    g_ = gridplot(tabs, ncols=ncols, plot_width=plot_width, plot_height=plot_height)
    show(g_)


def visualise_dashboard(U, df_cds, df_medians, output="../Data/dashboard.html", ncols=4, plot_width=250, plot_height=250, biodata=False, title="Dashboard", **kwargs):
    source = ColumnDataSource(df_cds)
    source2 = ColumnDataSource(df_medians)
    TOOLS = "box_select,lasso_select,box_zoom,reset,help"
    TOOLS2 = "hover, tap, pan, reset, wheel_zoom, box_zoom"

    names = []
    for u in U:
        name = None
        _pos = u[::-1].find('_')
        if _pos == -1:
            name = u
        elif _pos == 0:
            name = "Undefined"
        else:
            name = (u[-_pos:]).replace('|', '')
            if len(name) < 0:
                name = "Undefined"
        names.append(name)

    reset_output()
    output_file(output, title=title)

    # output_notebook()
    colors_bokeh = Category10[10]

    kmeans_plots = []
    if biodata:
        tooltips = [
            ("index", "$index"),
            ("Subject", "@subject"),
            ('Gestation Age', '@gestage'),
            ("Gender", "@gender"),
            ("Birth", "@birth"),
            ("Milk", "@milk"),
            ("Room", "@room"),
        ]
    else:
        tooltips = [
            ("index", "$index"),
            ("Subject", "@subject"),
        ]

    for i, OTU in enumerate(U):
        fig = figure(tools=TOOLS, title=names[i], background_fill_color="#fafafa", match_aspect=True)
        fig.circle("x{0}".format(i), "y{0}".format(i), size=10, color="kmeans{0}".format(i), hover_color="firebrick", source=source)
        hover = HoverTool(tooltips=tooltips)
        fig.add_tools(hover)
        kmeans_plots.append(fig)

    kmeans_gridplot = gridplot(kmeans_plots, ncols=ncols, plot_width=plot_width, plot_height=plot_height)
    kmeans_panel = Panel(child=kmeans_gridplot, title="K-Means Clustering")

    gmm_plots = []
    for i, OTU in enumerate(U):
        fig = figure(tools=TOOLS, title=names[i], background_fill_color="#fafafa", match_aspect=True)
        gmm = kwargs['gmm'][i]
        w_factor = 0.2 / gmm.weights_.max()
        x_ = []
        y_ = []
        w_ = []
        h_ = []
        angle_ = []
        for pos, covar, w in zip(gmm.means_, gmm.covariances_, gmm.weights_):
            xs, ys, ws, hs, angles = draw_ellipse(pos, covar, alpha=w * w_factor)
            x_.append(xs)
            y_.append(ys)
            w_.append(ws)
            h_.append(hs)
            angle_.append(angles)
        ellipse_source = ColumnDataSource(dict(x=[item for sublist in x_ for item in sublist], y=[item for sublist in y_ for item in sublist],
                                               w=[item for sublist in w_ for item in sublist], h=[item for sublist in h_ for item in sublist],
                                               angle=[item for sublist in angle_ for item in sublist]))
        glyph = Ellipse(x="x", y="y", width="w", height="h", angle="angle", fill_color="#cab2d6", fill_alpha=0.2, line_alpha=0)
        fig.add_glyph(ellipse_source, glyph)
        g_h = fig.circle("x{0}".format(i), "y{0}".format(i), size=5, color="gmm{0}".format(i), hover_color="firebrick", source=source)
        hover = HoverTool(tooltips=tooltips, renderers=[g_h])
        fig.add_tools(hover)
        gmm_plots.append(fig)

    gmm_gridplot = gridplot(gmm_plots, ncols=ncols, plot_width=plot_width, plot_height=plot_height)
    gmm_panel = Panel(child=gmm_gridplot, title="GMM Clustering")

    sil_plots = []
    for i, OTU in enumerate(U):
        #fig = figure(tools=TOOLS, title=U[i], background_fill_color="#fafafa", match_aspect=True)
        sil = kwargs['sils'][i]
        sil_err = kwargs['sils_err'][i]
        n_clusters = np.arange(2, len(sil)+2)
        errors = list(zip(n_clusters, sil, sil_err))
        a = hv.Curve(errors) * hv.ErrorBars(errors)
        a = a.opts(title=names[i], bgcolor="#fafafa", xlabel="No of Clusters", ylabel="Silhouette Value")
        fig = hv.render(a)
        sil_plots.append(fig)

    sil_gridplot = gridplot(sil_plots, ncols=ncols, plot_width=plot_width, plot_height=plot_height)
    sil_panel = Panel(child=sil_gridplot, title="Silhouette Scores (GMM)")

    if (biodata):
        bio_plots = []

        fig = figure(tools=TOOLS, title="Gestational Age vs Gender", background_fill_color="#fafafa")
        fig.circle("gestage", "gender", size=4, hover_color="firebrick", source=source)
        fig.add_tools(hover)
        fig.xaxis.axis_label = "Gestation Age"
        fig.yaxis.axis_label = "Gender"
        bio_plots.append(fig)

        fig = figure(tools=TOOLS, title="Gestational Age vs Milk", background_fill_color="#fafafa")
        fig.circle("gestage", "milk", size=4, hover_color="firebrick", source=source)
        fig.add_tools(hover)
        fig.xaxis.axis_label = "Gestation Age"
        fig.yaxis.axis_label = "Milk"
        bio_plots.append(fig)

        fig = figure(tools=TOOLS, title="Gestational Age vs Birth", background_fill_color="#fafafa")
        fig.circle("gestage", "birth", size=4, hover_color="firebrick", source=source)
        fig.add_tools(hover)
        fig.xaxis.axis_label = "Gestation Age"
        fig.yaxis.axis_label = "Birth"
        bio_plots.append(fig)

        fig = figure(tools=TOOLS, title="Gestational Age vs Room", background_fill_color="#fafafa")
        fig.circle("gestage", "room", size=4, hover_color="firebrick", source=source)
        fig.add_tools(hover)
        fig.xaxis.axis_label = "Gestation Age"
        fig.yaxis.axis_label = "Room"
        bio_plots.append(fig)

        g_3 = gridplot(bio_plots, ncols=ncols, plot_width=plot_width, plot_height=plot_height)
        biodata_panel = Panel(child=g_3, title="Biodata")

    resampled_plots = []
    for i, OTU in enumerate(U):
        fig = figure(tools=TOOLS2, title=names[i], background_fill_color="#fafafa")
        fig.xaxis.axis_label = "Temporal Axis"
        fig.yaxis.axis_label = "~ Abundance Axis"
        fig.multi_line("xs", "resampled{0}".format(i), hover_color="firebrick", nonselection_line_color="grey",
                       nonselection_line_alpha=0.1, source=source)
        fig.line("xs", "resampled{0}".format(i), line_color="deeppink", line_width=6, source=source2)
        resampled_plots.append(fig)
    resampled_gridplot = gridplot(resampled_plots, ncols=ncols, plot_width=plot_width, plot_height=plot_height)
    resampled_panel = Panel(child=resampled_gridplot, title="Resampled")

    source.selected.js_on_change('indices', CustomJS(args=dict(s=source, s2=source2, num_OTU=len(U)), code="""
        function median(numbers) {
            console.log('median');
            // median of [3, 5, 4, 4, 1, 1, 2, 3] = 3
            var median = 0, numsLen = numbers.length;
            numbers = numbers.sort(function(a, b) {
                return a - b;
            });
         
            if (
                numsLen % 2 === 0 // is even
            ) {
                // average of two middle numbers
                median = (numbers[numsLen / 2 - 1] + numbers[numsLen / 2]) / 2;
            } else { // is odd
                // middle number only
                median = numbers[(numsLen - 1) / 2];
            }
            console.log(numbers+" "+numsLen+" "+numbers[numsLen / 2 - 1]+" "+numbers[numsLen / 2]);
            console.log(median);
            return median;
        }
        
        const inds = s.selected.indices;
        const d = s.data;
        
        if (inds.length == 0)
            return;
        console.log(inds)
        
        var medians_array = new Array(num_OTU);
        var k;
        for (k = 0; k < num_OTU; k++) {
            var resampled = d['resampled'+k];
            var num_samples = resampled.length;
            var timepoints = resampled[0].length;
            console.log("resampled"+k);
            console.log(resampled);
            
            var median_local = new Array(timepoints);
            var i;
            for (i=0; i < timepoints; i++){
                var single_timepoint = [];
                var j;
                for (j in inds){
                    console.log("ind(inds[j]): "+ inds[j]+" time(i): "+i+" resampled[inds[j]][i]: "+ resampled[inds[j]][i]);
                    single_timepoint.push(resampled[inds[j]][i]);
                }
                single_timepoint = single_timepoint.flat();
                median_local[i] = median(single_timepoint);
            }
            medians_array[k] = median_local;
        }
        console.log(s.data);
        console.log(s2.data);
        
        for (k = 0; k < num_OTU; k++) {
            s2.data['resampled'+k] = medians_array[k];
        }
        
        console.log(medians_array);
        console.log(s2.data);
        
        s.change.emit();
        s2.change.emit();
        
    """))

    wavelet_plots = []
    for i, OTU in enumerate(U):
        fig = figure(tools=TOOLS2, title=names[i], background_fill_color="#fafafa")
        fig.xaxis.axis_label = "Temporal Axis"
        fig.yaxis.axis_label = "~ Abundance Axis"
        fig.multi_line("xs_dtw{0}".format(i), "ys_dtw{0}".format(i), hover_color="firebrick",
                       nonselection_line_color="grey", nonselection_line_alpha=0.1, source=source)
        wavelet_plots.append(fig)
    wavelet_gridplot = gridplot(wavelet_plots, ncols=ncols, plot_width=plot_width, plot_height=plot_height)
    wavelet_panel = Panel(child=wavelet_gridplot, title="Wavelet Transform")

    cluster_plots = []
    overlap = kwargs['overlap']
    hmap_o = hv.HeatMap(overlap)
    hmap_o.opts(opts.HeatMap(tools=['hover'], colorbar=True, width=plot_width, height=plot_height, toolbar='above', title='Overlap Coefficient', clim=(0, 1), xrotation=90))
    fig_o = hv.render(hmap_o)

    jaccard = kwargs['jaccard']
    hmap_j = hv.HeatMap(jaccard)
    hmap_j.opts(opts.HeatMap(tools=['hover'], colorbar=True, width=plot_width, height=plot_height, toolbar='above', title='Overlap Coefficient', clim=(0, 1), xrotation=90))
    fig_j = hv.render(hmap_j)

    cluster_plots = [fig_o, fig_j]
    cluster_gridplot = gridplot(cluster_plots, ncols=ncols, plot_width=plot_width, plot_height=plot_height)
    cluster_panel = Panel(child=cluster_gridplot, title="Cluster Membership")

    if biodata:
        right_pane = Tabs(tabs=[resampled_panel, wavelet_panel, biodata_panel, cluster_panel])
    else:
        right_pane = Tabs(tabs=[resampled_panel, wavelet_panel, cluster_panel])

    left_pane = Tabs(tabs=[gmm_panel, sil_panel, kmeans_panel])

    show(row([left_pane, right_pane]))

# adapted from https://github.com/vlavorini/ClusterCardinality
def draw_ellipse(position, covariance, **kwargs):
    """Draw an ellipse with a given position and covariance"""
    # Convert covariance to principal axes
    if covariance.shape == (2, 2):
        U, s, Vt = np.linalg.svd(covariance)
        angle = np.degrees(np.arctan2(U[1, 0], U[0, 0]))
        width, height = 2 * np.sqrt(s)
    else:
        angle = 0
        width, height = 2 * np.sqrt(covariance)

    xs = []
    ys = []
    ws = []
    hs = []
    angles = []

    # Draw the Ellipse
    for nsig in range(1, 4):
        xs.append(position[0])
        ys.append(position[1])
        ws.append(nsig*width)
        hs.append(nsig*height)
        angles.append(angle * math.pi / 180)
        #print(position[0], position[1], nsig * width, nsig * height, angle)
    return xs, ys, ws, hs, angles