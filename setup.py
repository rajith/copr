#!/usr/bin/env python

from distutils.core import setup

setup(name='Paper3',
      version='0.1',
      description='Programs for paper3',
      author='Rajith Vidanaarachchi',
      author_email='rajith.v@anu.edu.au',
      packages=['paper3'], requires=['pandas', 'matplotlib', 'numpy']
      )